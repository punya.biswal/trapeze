package auth

import (
	"net/http"

	"github.com/google/go-github/github"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
)

// Github is an auth implementation backed by public Github
type Github struct {
	oauth2.Config
}

// RedirectToAuthorize sends the client a redirect to the configured authorize
// URL (the first step in an OAuth flow)
func (s *Github) RedirectToAuthorize(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, s.AuthCodeURL("state"), http.StatusFound)
}

// GetUser fetches details about the user who previously generated an auth code
func (s *Github) GetUser(ctx context.Context, r *GetUserRequest) (*User, error) {
	token, err := s.Exchange(ctx, r.Code)
	if err != nil {
		return nil, err
	}

	client := github.NewClient(s.Client(ctx, token))

	user, _, err := client.Users.Get("")
	if err != nil {
		return nil, err
	}

	return &User{
		Id:    int64(*user.ID),
		Login: *user.Login,
	}, nil
}
