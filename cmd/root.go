package cmd

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"

	"github.com/golang/glog"
	"github.com/gorilla/handlers"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/spf13/cobra"
	"gitlab.com/punya.biswal/trapeze/auth"
	"gitlab.com/punya.biswal/trapeze/tasks"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/github"
	"google.golang.org/grpc"
)

var (
	clientID, clientSecret, databaseFilePath string
	grpcPort, httpPort                       int
)

// RootCmd is invoked when trapeze is launched
var RootCmd = &cobra.Command{
	Use:   "trapeze",
	Short: "Trapeze manages tasks",
	RunE: func(cmd *cobra.Command, args []string) error {
		authSrv := &auth.Github{
			Config: oauth2.Config{
				ClientID:     clientID,
				ClientSecret: clientSecret,
				Scopes:       []string{"user:email"},
				Endpoint:     github.Endpoint,
			}}

		taskSrv, err := tasks.Open(databaseFilePath)
		if err != nil {
			return err
		}
		defer warnOnError(taskSrv.Close)

		grpcAddress := fmt.Sprintf(":%d", grpcPort)
		lis, err := net.Listen("tcp", grpcAddress)
		if err != nil {
			return err
		}

		server := grpc.NewServer()
		tasks.RegisterServiceServer(server, taskSrv)
		auth.RegisterServiceServer(server, authSrv)
		go warnOnError(func() error { return server.Serve(lis) })

		mux := runtime.NewServeMux()
		opts := []grpc.DialOption{grpc.WithInsecure()}
		if err = auth.RegisterServiceHandlerFromEndpoint(context.Background(), mux, grpcAddress, opts); err != nil {
			return err
		}
		if err = tasks.RegisterServiceHandlerFromEndpoint(context.Background(), mux, grpcAddress, opts); err != nil {
			return err
		}

		http.Handle("/", mux)
		http.HandleFunc("/authorize", authSrv.RedirectToAuthorize)

		httpAddress := fmt.Sprintf(":%d", httpPort)
		handler := handlers.CombinedLoggingHandler(os.Stderr, http.DefaultServeMux)
		return http.ListenAndServe(httpAddress, handler)
	},
}

func init() {
	RootCmd.Flags().StringVar(&clientID, "clientID", "", "Github client ID")
	RootCmd.Flags().StringVar(&clientSecret, "clientSecret", "", "Github client secret")
	RootCmd.Flags().StringVar(&databaseFilePath, "databaseFilePath", "trapeze.db", "Database file path")
	RootCmd.Flags().IntVar(&grpcPort, "grpcPort", 8082, "gRPC port")
	RootCmd.Flags().IntVar(&httpPort, "httpPort", 8081, "HTTP port")
}

func warnOnError(fn func() error) {
	if err := fn(); err != nil {
		glog.Warning(err)
	}
}
