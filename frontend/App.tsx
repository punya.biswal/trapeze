import * as React from "react";
import { observer } from "mobx-react";
import { Auth, Error, Model, Tasks } from "./Model";

@observer
class TasksView extends React.Component<{ model: Tasks }, {}> {
    render() {
        let {user} = this.props.model;
        return (
            <div>
                <h1>Tasks for @{user.login}</h1>
                <form onSubmit={this.addTask}>
                    <input
                        type="text"
                        onChange={this.setTaskText}
                        value={this.props.model.taskText}
                        />
                    <input type="submit" value="Add task" />
                </form>
                <ul>
                    {this.getTasks()}
                </ul>
            </div>
        );
    }

    private getTasks() {
        return this.props.model.tasks.map((task, index) => (<li key={index}>{task.data.text} (#{task.id})</li>));
    }

    private addTask = (evt: React.FormEvent<HTMLFormElement>) => {
        evt.preventDefault();
        return this.props.model.addTask();
    }

    private setTaskText = (evt: React.FormEvent<HTMLInputElement>) => {
        this.props.model.setTaskText((evt.target as HTMLInputElement).value);
    }
}

const AuthView = ({}: { model: Auth }) => <h1>Auth</h1>;

const ErrorView = ({model}: { model: Error }) => (
    <div>
        <h1>Error</h1>
        <pre>{model.details}</pre>
    </div>
);

@observer
export class App extends React.Component<{ model: Model }, {}> {
    render() {
        let page = this.props.model.page;
        if (page instanceof Tasks) {
            return <TasksView model={page} />;
        } else if (page instanceof Auth) {
            return <AuthView model={page} />;
        } else if (page instanceof Error) {
            return <ErrorView model={page} />;
        }
        throw "Impossible";
    }
}
