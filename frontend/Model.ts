import { action, observable } from "mobx";
import { HTML5HistoryManager, StateNavigator } from "navigation";

export interface IUser {
    login: string;
}

export interface ITask {
    id: number;
    data: ITaskData;
}

export interface ITaskData {
    text: string;
}

export class Tasks {
    @observable taskText: string = "";
    @observable tasks: ITask[] = [];

    constructor(public user: IUser) {
        this.fetchTasks();
    }

    @action private async fetchTasks() {
        let response = await window.fetch("/tasks");
        if (!response.ok) {
            this.fetchTasksFailed();
        }
        this.fetchTasksSucceeded((await response.json()).tasks as ITask[]);
    }

    @action private fetchTasksFailed() {
        // nothing
    }

    @action private fetchTasksSucceeded(tasks: ITask[]) {
        this.tasks = tasks;
    }

    @action setTaskText(taskText: string) {
        this.taskText = taskText;
    }

    @action async addTask() {
        let text = this.taskText;
        let response = await window.fetch("/tasks", {
            method: "POST",
            body: JSON.stringify({ text }),
        });
        if (!response.ok) {
            this.addTaskFailed();
            return;
        }

        let id: number = (await response.json()).id;
        this.addTaskSucceeded({id, data: {text}});
    }

    @action private addTaskFailed() {
        // nothing so far
    }

    @action private addTaskSucceeded(task: ITask) {
        this.tasks.push(task);
        this.taskText = "";
    }
}

export class Auth { }

export class Error {
    constructor(public details: string) { }
}

export class Model {
    @observable page: Tasks | Auth | Error;

    private stateNavigator: StateNavigator;

    constructor() {
        this.stateNavigator = new StateNavigator([
            { key: "tasks", route: "" },
            { key: "auth", route: "auth" },
        ], new HTML5HistoryManager());

        this.stateNavigator.states["tasks"].navigated = () => {
            this.navigatedToTasks();
        };

        this.stateNavigator.states["auth"].navigated = ({code}) => {
            this.navigatedToAuth(code);
        };

        this.stateNavigator.start();
    }

    @action navigatedToTasks() {
        let userJSON = window.sessionStorage.getItem("user");
        if (typeof userJSON === "string") {
            let user = JSON.parse(userJSON);
            this.page = new Tasks(user);
            return;
        }

        let link = this.stateNavigator.getRefreshLink();
        window.sessionStorage.setItem("destination", link);
        window.location.href = "/authorize";
    }

    @action async navigatedToAuth(code: string) {
        this.page = new Auth();

        let response = await window.fetch(`/user?code=${code}`);
        if (!response.ok) {
            this.failedToFetchUser(await response.text());
        }

        let userJSON = await response.text();

        window.sessionStorage.setItem("user", userJSON);
        let destination = window.sessionStorage.getItem("destination") || "/";
        window.sessionStorage.removeItem("destination");
        this.stateNavigator.navigateLink(destination);
    }

    @action private failedToFetchUser(details: string) {
        this.page = new Error(details);
    }
}
