import * as React from "react";
import * as ReactDOM from "react-dom";
import { useStrict } from "mobx";
import DevTools from "mobx-react-devtools";
import { App } from "./App";
import { Model } from "./Model";

useStrict(true);

ReactDOM.render(
    <div>
        <DevTools />
        <App model={new Model()} />
    </div>,
    document.querySelector("#app")
);
