package tasks

import (
	"bytes"
	"encoding/binary"

	"github.com/boltdb/bolt"
	"github.com/golang/protobuf/proto"
	"golang.org/x/net/context"
)

var tasksBucketName = []byte("tasks")

// Bolt is a task store backed by a Bolt database file
type Bolt struct {
	*bolt.DB
}

// Open creates or opens a task store
func Open(filename string) (*Bolt, error) {
	db, err := bolt.Open(filename, 0600, nil)
	if err != nil {
		return nil, err
	}

	err = db.Update(func(tx *bolt.Tx) error {
		_, e2 := tx.CreateBucketIfNotExists(tasksBucketName)
		return e2
	})
	if err != nil {
		return nil, err
	}

	return &Bolt{db}, nil
}

// Add adds a task to the store
func (s *Bolt) Add(ctx context.Context, r *AddRequest) (*AddResponse, error) {
	result := &AddResponse{}

	err := s.Batch(func(tx *bolt.Tx) error {
		tasksBucket := tx.Bucket(tasksBucketName)

		var err error
		if result.Id, err = tasksBucket.NextSequence(); err != nil {
			return err
		}

		var key bytes.Buffer
		if err = binary.Write(&key, binary.BigEndian, result.Id); err != nil {
			return err
		}

		value, err := proto.Marshal(r.GetTask())
		if err != nil {
			return err
		}

		return tasksBucket.Put(key.Bytes(), value)
	})

	return result, err
}

// List fetches the list of all tasks in the store
func (s *Bolt) List(context.Context, *ListRequest) (*ListResponse, error) {
	result := &ListResponse{}

	err := s.View(func(tx *bolt.Tx) error {
		tasksBucket := tx.Bucket(tasksBucketName)

		return tasksBucket.ForEach(func(k []byte, v []byte) error {
			task := &Task{Data: &Data{}}

			key := bytes.NewBuffer(k)
			if err := binary.Read(key, binary.BigEndian, &task.Id); err != nil {
				return err
			}

			if err := proto.Unmarshal(v, task.Data); err != nil {
				return err
			}

			result.Tasks = append(result.Tasks, task)
			return nil
		})
	})

	return result, err
}
