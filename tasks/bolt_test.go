package tasks_test

import (
	"context"
	"io/ioutil"
	"os"
	"testing"

	"gitlab.com/punya.biswal/trapeze/tasks"
)

func TestTaskIO(t *testing.T) {
	must := func(err error) {
		if err != nil {
			t.Fatal(err)
		}
	}

	f, err := ioutil.TempFile("", "TestTaskIO")
	must(err)
	defer func() { must(os.Remove(f.Name())) }()

	ts, err := tasks.Open(f.Name())
	must(err)

	ctx := context.Background()
	messages := []string{"hello", "world"}

	for i, text := range messages {
		res, err := ts.Add(ctx, &tasks.AddRequest{
			Task: &tasks.Data{
				Text: text,
			},
		})
		must(err)

		expected := uint64(i) + 1
		if res.Id != expected {
			t.Errorf("Expected key %v but got %v", expected, res.Id)
		}
	}

	res, err := ts.List(ctx, &tasks.ListRequest{})
	must(err)

	if len(res.GetTasks()) != len(messages) {
		t.Errorf("Expected %d tasks but saw %d", len(messages), len(res.GetTasks()))
	}
	for i, v := range res.GetTasks() {
		expected := uint64(i) + 1
		if v.Id != expected {
			t.Errorf("Expected key %v but got %v", expected, v.Id)
		}

		if v.GetData().Text != messages[i] {
			t.Errorf("Expected text %v but got %v", messages[i], v.GetData().Text)
		}
	}
}
