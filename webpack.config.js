const webpack = require('webpack');

module.exports = {
    entry: './frontend/index.tsx',
    output: {
        filename: './bundle.js',
    },
    devtool: 'source-map',
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js'],
    },
    module: {
        loaders: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
            },
        ],
        preLoaders: [
            {
                test: /\.js$/,
                loaders: [
                    'react-hot-loader/webpack',
                    'source-map-loader',
                ],
            },
        ],
    },
    externals: {
        'react': 'React',
        'react-dom': 'ReactDOM',
        'mobx': 'mobx',
    },
    devServer: {
        contentBase: './frontend/',
        historyApiFallback: true,
        inline: true,
        proxy: {
            '/authorize': {
                target: 'http://localhost:8081',
            },
            '/tasks': {
                target: 'http://localhost:8081',
            },
            '/user': {
                target: 'http://localhost:8081',
            },
        },
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
    ],
};
